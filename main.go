package main

import (
	"database/sql"
	"fmt"
	"log"
	"net/http"

	"github.com/caarlos0/env"
	"github.com/gin-contrib/cors"
	"github.com/gin-contrib/static"
	"github.com/gin-gonic/gin"
	_ "github.com/mattn/go-sqlite3"
	"github.com/pkg/errors"
)

type Session struct {
	db        *sql.DB
	TableName string `env:"TABLE_NAME" envDefault:"atchoo"`
	DbPath    string `env:"DB_PATH" envDefault:"./db/counter.db"`
	ServeDir  string `env:"SERVE_DIR" envDefault:"./dist"`
}

func (session Session) noteEvent(event string) error {
	insertQuery := fmt.Sprintf("INSERT INTO %s (event) VALUES (?)", session.TableName)
	insertStatement, err := session.db.Prepare(insertQuery)
	if err != nil {
		return errors.WithStack(err)
	}
	_, err = insertStatement.Exec(event)
	return errors.WithStack(err)
}

func (session *Session) prepDB() error {
	var err error
	session.db, err = sql.Open("sqlite3", session.DbPath)
	createQuery := fmt.Sprintf("CREATE TABLE IF NOT EXISTS %s (id INTEGER PRIMARY KEY, event TEXT, timestamp DATETIME DEFAULT CURRENT_TIMESTAMP)", session.TableName)
	createTable, err := session.db.Prepare(createQuery)
	if err != nil {
		return errors.WithStack(err)
	}
	_, err = createTable.Exec()
	if err != nil {
		return errors.WithStack(err)
	}
	return nil
}

func (session Session) eventHandler(c *gin.Context) {
	err := session.noteEvent(c.Param("event"))
	if err != nil {
		log.Println(err)
		c.JSON(http.StatusInternalServerError, gin.H{"error": fmt.Sprintf("%+v", err)})
		return
	}
	c.JSON(http.StatusOK, gin.H{})
}

func main() {
	session := Session{}
	err := env.Parse(&session)
	if err != nil {
		log.Panic(err)
	}
	log.Printf("Running with session: %+v\n", session)
	if err := session.prepDB(); err != nil {
		log.Panic(err)
	}

	gin.SetMode(gin.ReleaseMode)
	router := gin.Default()
	corsConfig := cors.DefaultConfig()
	corsConfig.AllowAllOrigins = true
	router.Use(cors.New(corsConfig))

	router.Use(static.Serve("/", static.LocalFile(session.ServeDir, false)))
	router.GET("/event/:event", session.eventHandler)

	log.Fatal(router.Run())
}
