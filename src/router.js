import Vue from 'vue'
import Router from 'vue-router'
import Counter from './counter.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'counter',
      component: Counter
    }
  ]
})
